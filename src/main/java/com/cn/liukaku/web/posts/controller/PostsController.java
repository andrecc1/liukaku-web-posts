package com.cn.liukaku.web.posts.controller;


import com.cn.liukaku.web.posts.service.AdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
public class PostsController {

    @Resource
    private AdminService adminService;

    @RequestMapping(value = {"", "index"}, method = RequestMethod.GET)
    public String index() {

        return "index";
    }
}
