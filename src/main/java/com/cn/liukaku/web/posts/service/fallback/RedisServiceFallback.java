package com.cn.liukaku.web.posts.service.fallback;


import com.cn.liukaku.web.posts.service.RedisService;
import org.springframework.stereotype.Component;


@Component
public class RedisServiceFallback implements RedisService {

    @Override
    public String put(String key, String value, long seconds) {

        //20200626之前
        //return Fallback.badGateWay();
        return null;
    }

    @Override
    public String get(String key) {
        //20200626之前
        //return Fallback.badGateWay();

        return null;
    }
}
